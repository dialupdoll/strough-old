/*
 * the abstractions package contains abstract objects, in the C++ 
 * sense of the word: things that *lack* implementation.
 * this is important to have seperate, because S-I/O has a *lot* of
 * abstract code. It is intended to be a fully generic frontend, and
 * that means lots of abstract placeholder code to act as glue for
 * various plugins and builtins implementing said abstractions.
 */
package io.gitlab.dahlterm.abstractions;