package io.gitlab.dahlterm.abstractions.protocol;

import java.awt.Component;
import java.util.UUID;

import io.gitlab.dahlterm.abstractions.LayoutNode;
import io.gitlab.dahlterm.abstractions.events.AccountRegisteredEventReciever;
import java.io.FileInputStream;

public interface Protocol {
	//this will handle loading a dialog or other methods necessary to register an account with the application
	public void authorizeNewAccount(AccountRegisteredEventReciever arer);
	
	public UUID identifier();
	public String nickname();
	
	public String protocolShortDescription();

	public Component protocolSettingsConfigurationMenu();
	
	public Account loadRegisteredAccountFromDisk(String folder);
	
	public void saveToFile(Account Acc, String folder);
	
	public Account bySearchString(String nick, LayoutNode node);
}
