/*
 * abstractions.protocol stores all the abstractions that are extended
 * in order to implement a functional protocol into the frontend. if you
 * are making a protocol, you *must* implement everything in here in some
 * place, and it is recommended to structure your code with that in mind.
 */
package io.gitlab.dahlterm.abstractions.protocol;