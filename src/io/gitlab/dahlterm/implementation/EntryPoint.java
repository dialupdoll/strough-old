package io.gitlab.dahlterm.implementation;

import io.gitlab.dahlterm.abstractions.LayoutNode;
import io.gitlab.dahlterm.implementation.interfacing.Dashboard;
import io.gitlab.dahlterm.implementation.interfacing.GenericWindow;

public class EntryPoint {
	
	private static void iterateAndOpen(LayoutNode node) {
		if(node == null) {
			return;
		}
		
		if(node.openOnLaunch) {
			node.createWindowAndShow();
		}
		
		for(LayoutNode iter : node.children()) {
			iterateAndOpen(iter);
		}
	}
	public static void main(String[] args) {
		// this section will eventually set the look and feel requested by the user.
		try {
			//UIManager.setLookAndFeel(UIManager.);
			//MetalLookAndFeel.setCurrentTheme(new DefaultMetalTheme());
			//UIManager.setLookAndFeel(new MetalLookAndFeel());
		} catch (Exception e) {
			// handle exception
		}

		
		Dashboard d = new Dashboard();
		
		GenericWindow example = new GenericWindow("S-IO ALPHA Dashboard", d, true);
		example.setVisible(true);

		PersistentData profile = PersistentData.getUserProfile();
		// here we will open all layouts the user requests to open-on-launch
		iterateAndOpen(profile.displayLayouts());
		
		

		// TODO: make the plugin loader actually function lmao. seems like will
		// probably need an entirely different approach to plugin loading.
		
		// System.out.println("Hello World from the SIO entrypoint!");
		//
		// PluginLoader<Protocol> loader = new PluginLoader<Protocol>();
		// try {
		// Protocol xmpp = loader.LoadClass("/home/dahlterm/Documents/dev/sio/",
		// "io.dahlterm.plugins.xmpp.ProtocolXMPP", Protocol.class);
		// System.out.println(xmpp.ProtocolName("en_us"));
		// } catch (Exception e) {
		// e.printStackTrace();
		// }
	}
}
