package io.gitlab.dahlterm.implementation.interfacing;


import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
//import java.util.ArrayList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.UUID;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.Scrollable;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;

import io.gitlab.dahlterm.abstractions.Action;
import io.gitlab.dahlterm.abstractions.LayoutNode;
import io.gitlab.dahlterm.abstractions.ObjectCapabilities;
import io.gitlab.dahlterm.abstractions.events.EventReciever;
import io.gitlab.dahlterm.abstractions.protocol.Channel;
import io.gitlab.dahlterm.abstractions.protocol.ContentPage;
import io.gitlab.dahlterm.abstractions.protocol.Message;
import io.gitlab.dahlterm.implementation.PersistentData;
import io.gitlab.dahlterm.implementation.interfacing.default_components.DefaultComposeComponent;
import io.gitlab.dahlterm.implementation.interfacing.default_components.DefaultMessageComponent;

public class FeedView extends JPanel {

	public LayoutNode config = null;

	JSplitPane splitter = new JSplitPane();
	GenericSelectionList leftSide = null;
	VerticalScrollOnlyPanel scrollHandler = new VerticalScrollOnlyPanel(); // scroll pane why are you like this
	JScrollPane scrollPane = new JScrollPane(scrollHandler);
	DefaultComposeComponent composeMessage;
	JPanel rightSide = new JPanel();

	Channel target = null;

	ContentPage page = null;
	
	JLabel feedName = new JLabel(), feedTopic = new JLabel();
	
	JLabel layoutEmptyChannel = new JLabel("No channel endpoint to display! Check your layout editor in the dashboard., if this is in error.");
	JLabel layoutEmptyChildren = new JLabel("No child channels to display! You probably set the wrong layout node to open, or opened the wrong layout node manually. Check your layout editor in the dashboard, if this is in error.");
	JButton feedMenu = new JButton("...");
	
	ArrayList<Component> messageComponents = new ArrayList<Component>();

	public FeedView(LayoutNode configuration) {
		config = configuration;
		updateLayoutView();
		splitter.setLeftComponent(leftSide);
		if(configuration.children() == null | configuration.children().isEmpty()) {
			splitter.setLeftComponent(layoutEmptyChildren);
		}
		splitter.setRightComponent(rightSide);
		setMinimumSize(new Dimension(300, 250));
		setLayout(new GridLayout(1,1));	/// this makes it scale right and fill the space.
		add(splitter);
	}
	FeedPanel fp = null;
	public void setFocus(LayoutNode focus) {
		
		target = focus.endpoint();
		
		if(target == null) {

			rightSide.add(layoutEmptyChannel);
			return;
		} else {
			fp = new FeedPanel(target);
			rightSide.add(fp);
			target.setUpdateReciever(fp);
		}
		
		
		splitter.setRightComponent(rightSide);
		
	}


	LayoutNode selectedLayoutNode = null;

	public void updateLayoutView() {
		if(leftSide == null) {
			leftSide = new GenericSelectionList(layoutCapList());
		}
	
		rightSide.removeAll();
		if(selectedLayoutNode != null) {
			setFocus(selectedLayoutNode);
		} else {
			rightSide.setLayout(new GridLayout(1, 1));
			rightSide.add(layoutEmptyChannel);
		}
		leftSide.wrapped = layoutCapList();
		leftSide.updateComponents();
		
		redraw(leftSide);
		redraw(rightSide);
	}
	
	private void redraw(Component c) {
		c.repaint();
		c.revalidate();
	}
	
	public void addActionsFromLayoutNode(LayoutNode node, ArrayList<Action> results) {
		if(node.children() == null) return;
		for(LayoutNode entry : node.children()) {
			final LayoutNode f_entry = entry;
			results.add(new Action() {
				int depth = layersDeep;
			public void performAction(ObjectCapabilities target) {
					selectedLayoutNode = f_entry;
					updateLayoutView();
				}
				
				public String helpInformation() {
					return "" + f_entry.identity().toString();		//not sure what else to put here, yet
				}
				
				public String actionTitle() {
					String pretext = "~ ";
					
					String title = "";
					if(f_entry.nodeTitle() == null)
						title = f_entry.identity().toString().substring(0, 6);
					else if(f_entry.nodeTitle().equals(""))
						title = f_entry.identity().toString().substring(0, 6);
					else
						title = f_entry.nodeTitle();
					
					return (String.join("", Collections.nCopies(depth, pretext)) + " " + title);
				}

				public UUID identifier() {
					return f_entry.identity();
				}
			});
			layersDeep++;
			addActionsFromLayoutNode(entry, results);
			layersDeep--;
		}
	}
	private int layersDeep = 0;
	public ObjectCapabilities layoutCapList() {
		layersDeep = 0;
		return new ObjectCapabilities() {
			
			public Action[] properties() {
				LayoutNode registered = config;
				
				ArrayList<Action> results = new ArrayList<Action>();
				
				addActionsFromLayoutNode(registered, results);
				
				if(results.isEmpty()) {
					System.out.println("no channels or layouts to display!");
					return null;
				} else {
					return results.toArray(new Action[1]);
				}
			}
		};
	}

	
	private class FeedPanel extends JPanel implements EventReciever {

		VerticalScrollOnlyPanel scrollHandler = new VerticalScrollOnlyPanel(); // scroll pane why are you like this
		JScrollPane scrollPane = new JScrollPane(scrollHandler);
		DefaultComposeComponent composeMessage;

		Channel target = null;

		ContentPage page = null;
		
		JLabel feedName = new JLabel(), feedTopic = new JLabel();
		JButton feedMenu = new JButton("...");
		
		ArrayList<Component> messageComponents = new ArrayList<Component>();

		public FeedPanel(Channel targetChan) {
			
			setMinimumSize(new Dimension(300, 250));
			
			scrollHandler.setLayout(new BoxLayout(scrollHandler, BoxLayout.Y_AXIS));
			scrollHandler.setAlignmentX(Component.LEFT_ALIGNMENT);
			scrollHandler.setAlignmentY(Component.BOTTOM_ALIGNMENT);
			scrollHandler.setBorder(BorderFactory.createEmptyBorder());
			
			scrollPane.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
			
			target = targetChan;
			
			targetChan.setUpdateReciever(this);

			JPanel topBar = new JPanel();
			topBar.setLayout(new BorderLayout());
			topBar.add(feedTopic, BorderLayout.CENTER);
			topBar.add(feedName, BorderLayout.WEST);
			topBar.add(feedMenu, BorderLayout.EAST);
			feedName.setBorder(new EmptyBorder(0, 4, 0, 4));
			feedTopic.setBorder(new EmptyBorder(0, 4, 0, 4));
			
			scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			setLayout(new BorderLayout());
			add(scrollPane, BorderLayout.CENTER);
			if (target != null) {
				composeMessage = new DefaultComposeComponent(target);
			}
			add(composeMessage, BorderLayout.SOUTH);
			add(topBar, BorderLayout.NORTH);
			
			feedMenu.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					if(target.properties() != null) {
						JPopupMenu menu = PopupMenus.createMenu(target, "en-us");
						menu.show(feedMenu, 0, feedMenu.getHeight());
					}
				}
				
			});
			
			updateChat();
			
		}

		public void addMessages(ContentPage contents) {
			Message[] messages = contents.contents();
			
			if(messages == null) return;
			for (Message msg : messages) {
				DefaultMessageComponent mc = new DefaultMessageComponent(msg);
				msg.setUpdateReciever(this);
				
				mc.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
				scrollHandler.add(mc);
				messageComponents.add((Component)mc);
			}
		}

		public boolean isScrolledBackward() {
			return false;
		}

		public void scrollToPresent() {
			if (scrollPane == null)
				return;
			JScrollBar vertical = scrollPane.getVerticalScrollBar();
			vertical.setValue(vertical.getMaximum());
		}

		public void updateChat() {

			feedName.setText(target.title());
			feedName.setToolTipText(target.title());
			redraw(feedName);
			
			feedTopic.setText(target.topic());
			feedTopic.setToolTipText(target.topic());
			redraw(feedTopic);
			
			Font f = feedTopic.getFont();
			feedTopic.setFont(f.deriveFont(f.getStyle() & ~Font.BOLD));
			
			page = target.retrievePage(null);
			target.setUpdateReciever(this);
			scrollHandler.removeAll();
			addMessages(page);
			scrollPane.repaint();
			scrollPane.revalidate();
			for(Component c : messageComponents) {
				c.repaint();
				c.revalidate();
			}
			redraw(composeMessage);

			// hacky method to make sure to update the scroll pane after it is finished.
			// not sure how else to do it yet.
			Thread thread = new Thread() {
				public void run() {
					try {
						sleep(20);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					if (!isScrolledBackward()) {
						scrollToPresent();
						for(Component c : messageComponents) {
							redraw(c);
						}
					}
				}
			};

			thread.start();

		}

		public void receiveEvent(Object event) {
			updateChat();
		}
	}


}
