/*
 * This is currently the only built-in protocol, which really
 * isn't a protocol in the internet sense. Instead, it is a
 * protocol between the user and the file system, for storing
 * simple "lists" of items.
 */
package io.gitlab.dahlterm.implementation.protocols.listpad;